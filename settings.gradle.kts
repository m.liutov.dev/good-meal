include(":app")
include(":data:meals")
include(":data:user")
include(":feature:meals")
include(":core:network")
include(":core:model")
include(":core:compose")
include(":core:presentation")
include(":core:common")

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

rootProject.name = "GoodMeal"

pluginManagement {
    includeBuild("build-logic")
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}
include(":domain:meals")
