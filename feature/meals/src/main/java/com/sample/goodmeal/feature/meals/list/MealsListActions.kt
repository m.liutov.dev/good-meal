package com.sample.goodmeal.feature.meals.list

internal interface MealsListActions : MealsListToolbarActions {
    fun onMealPressed(mealId: String)
    fun onRetryLoading()
}

internal interface MealsListToolbarActions {
    fun onSearchPressed()
    fun onSearchTermChanged(newTerm: String)
    fun onSearchFocusChanged(newIsFocused: Boolean)
    fun onClearSearchPressed()
}