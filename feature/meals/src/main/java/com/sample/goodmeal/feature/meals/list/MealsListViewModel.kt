package com.sample.goodmeal.feature.meals.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sample.goodmeal.core.presentation.error.ErrorMapper
import com.sample.goodmeal.core.presentation.error.catchDomainException
import com.sample.goodmeal.core.presentation.event.SingleLiveEvent
import com.sample.goodmeal.core.presentation.event.immutable
import com.sample.goodmeal.core.presentation.viewmodel.stateInVmWithInitial
import com.sample.goodmeal.domain.meals.GetUserMealsListUseCase
import com.samplegoodmeal.core.model.Meal
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.transform
import javax.inject.Inject
import kotlin.time.Duration.Companion.seconds

@OptIn(FlowPreview::class)
@HiltViewModel
internal class MealsListViewModel @Inject constructor(
    private val getUserMealsList: GetUserMealsListUseCase,
    private val errorMapper: ErrorMapper,
) : ViewModel(), MealsListActions {

    private val mealsList = MutableStateFlow<List<Meal>?>(null)
    private val searchTerm = MutableStateFlow("")

    private val isSearchFocused = MutableStateFlow(false)
    private val isSearchVisible = MutableStateFlow(false)

    private val loadingError = MutableStateFlow<String?>(null)

    private val filteredMeals = searchTerm.debounce(SEARCH_DEBOUNCE).transform {
        emit(null)
        emit(getUserMealsList(it))
    }

    private val contentState = combine(mealsList, loadingError) { meals, error ->
        when {
            error != null -> MealsListError(error)
            meals != null -> MealsListContent(meals)
            else -> MealsListProgress
        }
    }

    private val toolbarState = combine(searchTerm, isSearchVisible) { term, isVisible ->
        MealsTopBarState(term, isVisible)
    }

    val viewState = combine(toolbarState, contentState) { toolbar, content ->
        MealsListViewState(toolbar, content)
    }.stateInVmWithInitial(null)

    private val _eventNavigateDetails = SingleLiveEvent<String>()
    val eventNavigateDetails = _eventNavigateDetails.immutable()

    init {
        loadMeals()
        setupFocusListener()
    }

    private fun setupFocusListener() {
        isSearchFocused.onEach { isFocused ->
            if (!isFocused && searchTerm.value.isBlank()) {
                isSearchVisible.value = false
            }
        }.launchIn(viewModelScope)
    }

    private fun loadMeals() = filteredMeals.onEach { newMeals ->
        loadingError.value = null
        mealsList.value = newMeals
    }.catchDomainException {
        loadingError.value = errorMapper.mapErrorMessage(it)
    }.launchIn(viewModelScope)

    override fun onRetryLoading() {
        loadMeals()
    }

    override fun onMealPressed(mealId: String) {
        _eventNavigateDetails.value = mealId
    }

    override fun onSearchPressed() {
        isSearchVisible.value = true
    }

    override fun onSearchTermChanged(newTerm: String) {
        searchTerm.value = newTerm
    }

    override fun onSearchFocusChanged(newIsFocused: Boolean) {
        isSearchFocused.value = newIsFocused
    }

    override fun onClearSearchPressed() {
        searchTerm.value = ""
        isSearchVisible.value = false
    }

    companion object {
        private val SEARCH_DEBOUNCE = 1.seconds
    }
}