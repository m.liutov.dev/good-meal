package com.sample.goodmeal.feature.meals.details

import com.samplegoodmeal.core.model.DetailedMeal

internal sealed interface MealDetailsViewState

internal object MealDetailsProgress : MealDetailsViewState
internal data class MealDetailsError(val errorMessage: String) : MealDetailsViewState
internal data class MealDetailsContent(
    val meal: DetailedMeal,
) : MealDetailsViewState