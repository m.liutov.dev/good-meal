package com.sample.goodmeal.feature.meals.details

internal interface MealDetailsActions {
    fun onBackPressed()
    fun onOpenVideoPressed()
    fun onRetryLoading()
}