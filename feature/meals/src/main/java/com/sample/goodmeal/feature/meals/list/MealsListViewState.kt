package com.sample.goodmeal.feature.meals.list

import com.samplegoodmeal.core.model.Meal

internal data class MealsListViewState(
    val topBarState: MealsTopBarState,
    val contentState: MealsListContentState,
)

internal data class MealsTopBarState(
    val searchTerm: String,
    val isSearchVisible: Boolean,
)

internal sealed interface MealsListContentState

internal object MealsListProgress : MealsListContentState
internal data class MealsListError(val errorMessage: String) : MealsListContentState
internal data class MealsListContent(
    val meals: List<Meal>,
) : MealsListContentState