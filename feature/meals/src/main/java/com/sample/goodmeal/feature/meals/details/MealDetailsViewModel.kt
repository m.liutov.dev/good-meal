package com.sample.goodmeal.feature.meals.details

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sample.goodmeal.core.presentation.error.ErrorMapper
import com.sample.goodmeal.core.presentation.error.catchDomainException
import com.sample.goodmeal.core.presentation.event.SingleLiveEvent
import com.sample.goodmeal.core.presentation.event.immutable
import com.sample.goodmeal.core.presentation.viewmodel.stateInVmWithInitial
import com.sample.goodmeal.data.meals.MealsRepository
import com.samplegoodmeal.core.model.DetailedMeal
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
internal class MealDetailsViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val mealsRepository: MealsRepository,
    private val errorMapper: ErrorMapper,
) : ViewModel(), MealDetailsActions {

    private val args = MealDetailsFragmentArgs.fromSavedStateHandle(savedStateHandle)
    private val mealId: String = args.mealId

    private val detailedMeal = MutableStateFlow<DetailedMeal?>(null)

    private val loadingError = MutableStateFlow<String?>(null)

    val viewState = combine(detailedMeal, loadingError) { meal, error ->
        when {
            error != null -> MealDetailsError(error)
            meal != null -> MealDetailsContent(meal)
            else -> MealDetailsProgress
        }
    }.stateInVmWithInitial(MealDetailsProgress)

    private val _eventOpenVideo = SingleLiveEvent<String>()
    val eventOpenVideo = _eventOpenVideo.immutable()

    private val _eventNavigateBack = SingleLiveEvent<Unit>()
    val eventNavigateBack = _eventNavigateBack.immutable()

    init {
        loadMeal()
    }

    private fun loadMeal() {
        mealsRepository.findMealById(mealId).onEach {
            loadingError.value = null
            detailedMeal.value = it
        }.catchDomainException {
            loadingError.value = errorMapper.mapErrorMessage(it)
        }.launchIn(viewModelScope)
    }

    override fun onRetryLoading() = loadMeal()

    override fun onOpenVideoPressed() {
        viewModelScope.launch {
            detailedMeal.firstOrNull()?.let {
                _eventOpenVideo.value = it.videoUrl
            }
        }
    }

    override fun onBackPressed() {
        _eventNavigateBack.call()
    }
}