package com.sample.goodmeal.feature.meals.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.sample.goodmeals.core.compose.setThemedContent
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
internal class MealsListFragment : Fragment() {

    private val viewModel: MealsListViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View = setThemedContent {
        MealsListScreen()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initEventObservers()
    }

    private fun initEventObservers() {
        viewModel.eventNavigateDetails.observe(viewLifecycleOwner) {
            findNavController().navigate(MealsListFragmentDirections.actionToDetails(it))
        }
    }
}