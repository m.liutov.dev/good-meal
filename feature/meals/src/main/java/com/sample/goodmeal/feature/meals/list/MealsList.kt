package com.sample.goodmeal.feature.meals.list

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.AsyncImage
import com.sample.goodmeal.feature.meals.R
import com.sample.goodmeals.core.compose.ClearFocusOnKeyboardDismiss
import com.sample.goodmeals.core.compose.ErrorBox
import com.sample.goodmeals.core.compose.ui.GoodMealTheme
import com.samplegoodmeal.core.model.Meal

@Composable
internal fun MealsListScreen(viewModel: MealsListViewModel = viewModel()) {
    val state = viewModel.viewState.collectAsStateWithLifecycle()
    state.value?.let {
        ScreenInternal(it, viewModel)
    }
}

@Composable
@Preview
private fun ScreenPreview() {
    GoodMealTheme {
        ScreenInternal(
            state = MealsListViewState(
                MealsTopBarState("", false),
                MealsListContent(
                    listOf(
                        Meal("1", "Meal 1", ""),
                        Meal("2", "Meal 2", ""),
                        Meal("3", "Meal 3", ""),
                        Meal("4", "Meal 4", ""),
                    )
                )
            ),
            object : MealsListActions {
                override fun onMealPressed(mealId: String) {}
                override fun onSearchPressed() {}
                override fun onSearchTermChanged(newTerm: String) {}
                override fun onSearchFocusChanged(newIsFocused: Boolean) {}
                override fun onClearSearchPressed() {}
                override fun onRetryLoading() {}
            }
        )
    }
}

@Composable
private fun ScreenInternal(state: MealsListViewState, actions: MealsListActions) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.background)
    ) {
        TopBar(state.topBarState, actions)
        Box(modifier = Modifier.fillMaxSize()) {
            when (val content = state.contentState) {
                is MealsListContent -> ScreenContent(
                    content = content, actions = actions, modifier = Modifier.fillMaxSize()
                )
                MealsListProgress -> ScreenProgress(modifier = Modifier.align(Alignment.Center))
                is MealsListError -> ErrorBox(
                    message = content.errorMessage,
                    onRetry = actions::onRetryLoading,
                    modifier = Modifier.align(Alignment.Center)
                )
            }
        }
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun TopBar(state: MealsTopBarState, actions: MealsListToolbarActions) {
    val inputFocusRequester = remember {
        FocusRequester()
    }
    AnimatedContent(
        targetState = state.isSearchVisible,
        modifier = Modifier
            .fillMaxWidth()
            .background(MaterialTheme.colors.primary)
            .padding(vertical = 8.dp)
    ) {
        Box {
            if (it) {
                Row(
                    horizontalArrangement = Arrangement.spacedBy(16.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    TopBarSearchField(
                        state.searchTerm, inputFocusRequester, actions,
                        modifier = Modifier.weight(1f, fill = true)
                    )
                    IconButton(
                        onClick = actions::onClearSearchPressed,
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_close),
                            contentDescription = stringResource(
                                id = R.string.clear_search_content_desc
                            ),
                            modifier = Modifier.size(
                                dimensionResource(id = R.dimen.meals_list_search_icon)
                            )
                        )
                    }
                }
            } else {
                Text(
                    text = stringResource(id = R.string.meals_list_title),
                    modifier = Modifier.align(Alignment.Center),
                    style = MaterialTheme.typography.h4,
                    color = MaterialTheme.colors.onPrimary
                )
                IconButton(
                    onClick = actions::onSearchPressed,
                    modifier = Modifier.align(Alignment.CenterEnd)
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_search),
                        contentDescription = stringResource(
                            id = R.string.search_content_desc
                        ),
                        modifier = Modifier.size(
                            dimensionResource(id = R.dimen.meals_list_search_icon)
                        )
                    )
                }
            }
        }
    }
    ClearFocusOnKeyboardDismiss()
    LaunchedEffect(key1 = state.isSearchVisible) {
        if (state.isSearchVisible) {
            inputFocusRequester.requestFocus()
        }
    }
}

@Composable
private fun TopBarSearchField(
    term: String,
    focusRequester: FocusRequester,
    actions: MealsListToolbarActions,
    modifier: Modifier = Modifier,
) {
    TextField(
        value = term,
        onValueChange = {
            actions.onSearchTermChanged(it)
        },
        placeholder = {
            Text(text = stringResource(id = R.string.meals_list_search_placeholder))
        },
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = Color.Transparent,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent,
            cursorColor = MaterialTheme.colors.onPrimary,
            textColor = MaterialTheme.colors.onPrimary,
            placeholderColor = MaterialTheme.colors.onPrimary
        ),
        keyboardOptions = KeyboardOptions(capitalization = KeyboardCapitalization.Sentences),
        modifier = modifier
            .padding(top = 8.dp, start = 8.dp, end = 8.dp)
            .onFocusChanged {
                actions.onSearchFocusChanged(it.isFocused)
            }
            .focusRequester(focusRequester),
    )
}

@Composable
private fun ScreenProgress(modifier: Modifier = Modifier) {
    CircularProgressIndicator(modifier = modifier)
}

@Composable
private fun ScreenContent(
    content: MealsListContent,
    actions: MealsListActions,
    modifier: Modifier = Modifier,
) {
    LazyColumn(
        modifier = modifier,
        contentPadding = PaddingValues(bottom = 16.dp)
    ) {
        itemsIndexed(content.meals, key = { _, item -> item.id }) { index, item ->
            MealItem(
                item = item, modifier = Modifier
                    .fillMaxWidth()
                    .clickable(onClick = { actions.onMealPressed(item.id) })
                    .padding(16.dp)
            )
            if (index != content.meals.lastIndex) {
                Divider(color = Color.LightGray)
            }
        }
    }
}

@Composable
private fun MealItem(item: Meal, modifier: Modifier = Modifier) {
    Row(
        horizontalArrangement = Arrangement.spacedBy(16.dp, Alignment.Start),
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
    ) {
        AsyncImage(
            model = item.imageUrl, contentDescription = item.title,
            modifier = Modifier
                .size(dimensionResource(id = R.dimen.meals_list_meal_icon))
                .clip(MaterialTheme.shapes.medium)
        )
        Text(text = item.title, style = MaterialTheme.typography.h6)
    }
}