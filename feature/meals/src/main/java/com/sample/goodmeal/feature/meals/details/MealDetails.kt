package com.sample.goodmeal.feature.meals.details

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.AsyncImage
import com.sample.goodmeal.feature.meals.R
import com.sample.goodmeals.core.compose.ErrorBox
import com.sample.goodmeals.core.compose.ui.GoodMealTheme
import com.samplegoodmeal.core.model.DetailedMeal

private const val ROTATION_NORMAL = 0f
private const val ROTATION_FLIPPED = 180f
private const val BACK_BTN_ALPHA = 0.7f

@Composable
internal fun MealDetailsScreen(viewModel: MealDetailsViewModel = viewModel()) {
    val state = viewModel.viewState.collectAsStateWithLifecycle()
    ScreenInternal(state.value, viewModel)
}

@Composable
private fun ScreenInternal(state: MealDetailsViewState, actions: MealDetailsActions) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.background)
    ) {
        when (state) {
            is MealDetailsContent -> ScreenContent(
                content = state,
                actions = actions,
                modifier = Modifier.fillMaxSize()
            )
            MealDetailsProgress -> ScreenProgress(modifier = Modifier.align(Alignment.Center))
            is MealDetailsError -> ErrorBox(
                message = state.errorMessage,
                onRetry = actions::onRetryLoading,
                modifier = Modifier.align(Alignment.Center)
            )
        }
        BackButton(actions::onBackPressed)
    }
}

@Composable
@Preview
private fun ScreenPreview() {
    GoodMealTheme {
        ScreenInternal(
            state = MealDetailsContent(
                DetailedMeal(
                    "1", "Detailed meal", "img", "", "Instruction on how to cook it.",
                    emptyList()
                )
            ),
            object : MealDetailsActions {
                override fun onBackPressed() {}
                override fun onOpenVideoPressed() {}
                override fun onRetryLoading() {}
            }
        )
    }
}

@Composable
private fun BackButton(onBackPressed: () -> Unit) {
    IconButton(onClick = onBackPressed) {
        Box(
            modifier = Modifier
                .background(MaterialTheme.colors.surface.copy(alpha = BACK_BTN_ALPHA), CircleShape)
                .padding(4.dp)
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_back_arrow),
                contentDescription = stringResource(id = R.string.back_content_desc),
                modifier = Modifier.size(dimensionResource(id = R.dimen.meals_details_back_icon))
            )
        }
    }
}

@Composable
private fun ScreenProgress(modifier: Modifier = Modifier) {
    CircularProgressIndicator(modifier = modifier)
}

@Composable
private fun ScreenContent(
    content: MealDetailsContent,
    actions: MealDetailsActions,
    modifier: Modifier = Modifier,
) {
    val meal = content.meal
    Column(
        modifier = modifier.verticalScroll(rememberScrollState())
    ) {
        AsyncImage(
            model = meal.imageUrl, contentDescription = meal.title,
            modifier = Modifier
                .fillMaxWidth()
                .aspectRatio(4 / 3f)
                .align(Alignment.CenterHorizontally),
            contentScale = ContentScale.Crop
        )
        Spacer(modifier = Modifier.height(48.dp))
        Text(
            text = meal.title,
            style = MaterialTheme.typography.h3,
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(horizontal = 16.dp)
        )
        Spacer(modifier = Modifier.height(16.dp))
        ExpandableSection(
            stringResource(id = R.string.meal_details_instructions_title),
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(text = meal.instructions)
        }
        ExpandableSection(
            stringResource(id = R.string.meal_details_video_title),
            modifier = Modifier.fillMaxWidth()
        ) {
            Button(
                onClick = actions::onOpenVideoPressed
            ) {
                Text(
                    text = stringResource(id = R.string.meal_details_open_video_btn),
                    style = MaterialTheme.typography.button
                )
            }
        }
    }
}

@Composable
private fun ExpandableSection(
    title: String,
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit,
) {
    var isExpanded by rememberSaveable {
        mutableStateOf(false)
    }
    val arrowRotation = if (isExpanded) ROTATION_FLIPPED else ROTATION_NORMAL
    Column(modifier = modifier.padding(horizontal = 16.dp)) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.spacedBy(16.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(text = title, style = MaterialTheme.typography.h6)
            IconButton(
                onClick = { isExpanded = !isExpanded },
            ) {
                val animatedRotation = animateFloatAsState(targetValue = arrowRotation)
                Image(
                    painter = painterResource(id = R.drawable.ic_expand),
                    contentDescription = stringResource(
                        id = R.string.meal_details_expand_collapse_content_desc
                    ),
                    modifier = Modifier
                        .size(
                            dimensionResource(id = R.dimen.meals_details_expand_icon)
                        )
                        .rotate(animatedRotation.value)
                )
            }
        }
        AnimatedVisibility(visible = isExpanded) {
            content()
        }
        Spacer(modifier = Modifier.height(8.dp))
    }
}
