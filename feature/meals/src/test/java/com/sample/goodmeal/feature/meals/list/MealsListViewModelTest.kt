package com.sample.goodmeal.feature.meals.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.asFlow
import app.cash.turbine.test
import com.sample.goodmeal.core.presentation.error.ErrorMapper
import com.sample.goodmeal.domain.meals.GetUserMealsListUseCase
import com.sample.goodmeal.feature.meals.MockErrorMapper
import com.samplegoodmeal.core.model.Meal
import com.samplegoodmeal.core.model.NoInternetException
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MealsListViewModelTest {
    private val errorMapper: ErrorMapper = MockErrorMapper()
    private lateinit var getUserMealsList: GetUserMealsListUseCase

    private lateinit var viewModel: MealsListViewModel

    private val testDispatcher = UnconfinedTestDispatcher()

    @get:Rule
    val taskExecutorRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
        getUserMealsList = mockk()
    }

    private fun setRepoSuccess() {
        coEvery { getUserMealsList(SEARCH_TERM_NONE) } returns listOf(MEAL_NO_TERM)
        coEvery { getUserMealsList(SEARCH_TERM_SOME) } returns listOf(MEAL_SOME_TERM)
    }

    private fun setRepoError() {
        coEvery { getUserMealsList(SEARCH_TERM_NONE) } throws NoInternetException()
        coEvery { getUserMealsList(SEARCH_TERM_SOME) } throws NoInternetException()
    }

    private fun initVm() {
        viewModel = MealsListViewModel(getUserMealsList, errorMapper)
    }

    @After
    fun cleanUp() {
        Dispatchers.resetMain()
    }

    @Test
    fun repoSlowReturn_progressState() = runTest {
        val searchCompletable = CompletableDeferred<Unit>()
        // Delay returning until after first state is captured in test
        coEvery { getUserMealsList(SEARCH_TERM_NONE) } coAnswers {
            searchCompletable.await()
            emptyList()
        }

        initVm()

        viewModel.viewState.test {
            val state = awaitItem()
            assertEquals(MealsListProgress, state?.contentState)
            // Test is done, can allow repo to return now
            searchCompletable.complete(Unit)
            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    fun repoException_errorState() = runTest {
        setRepoError()

        initVm()

        viewModel.viewState.test {
            val progressState = awaitItem()?.contentState
            assert(progressState is MealsListProgress) { "Expected MealsListProgress" }

            val errorState = awaitItem()?.contentState
            assert(errorState is MealsListError) { "Expected MealsListError" }
            val actualErrorMessage = (errorState as MealsListError).errorMessage

            assertEquals(MockErrorMapper.ERROR_MESSAGE, actualErrorMessage)
            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    fun repoSuccess_contentState() = runTest {
        setRepoSuccess()

        initVm()

        viewModel.viewState.map { it?.contentState }.test {
            val progressState = awaitItem()
            assert(progressState is MealsListProgress) { "Expected MealsListProgress" }

            val contentState = awaitItem()
            assert(contentState is MealsListContent) { "Expected MealsListContent" }
            val actualContent = (contentState as MealsListContent).meals

            assertEquals(listOf(MEAL_NO_TERM), actualContent)
            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    fun repoErrorThenSuccess_errorRetryContentState() = runTest {
        setRepoError()

        initVm()

        viewModel.viewState.map { it?.contentState }.test {
            val progressState = awaitItem()
            assert(progressState is MealsListProgress) { "Expected MealsListProgress" }

            val errorState = awaitItem()
            assert(errorState is MealsListError) { "Expected MealsListError" }

            setRepoSuccess()
            viewModel.onRetryLoading()

            val contentState = awaitItem()
            assert(contentState is MealsListContent) { "Expected MealsListContent" }
            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    fun repoSuccess_changeTerm_newMealEmitted() = runTest {
        setRepoSuccess()

        initVm()

        viewModel.viewState.map { it?.contentState }.test {
            val progressState = awaitItem()
            assert(progressState is MealsListProgress) { "Expected MealsListProgress" }

            val contentState = awaitItem()
            assert(contentState is MealsListContent) { "Expected MealsListContent" }

            viewModel.onSearchTermChanged(SEARCH_TERM_SOME)
            // Advancing time to handle VM-s debounce, which is not skipped in test
            advanceTimeBy(2000)

            val newContentState = expectMostRecentItem()
            assert(newContentState is MealsListContent) { "Expected MealsListContent" }
            val newActualContent = (newContentState as MealsListContent).meals
            assertEquals(listOf(MEAL_SOME_TERM), newActualContent)
            cancelAndIgnoreRemainingEvents()
        }
    }

    private fun assertTopBarInvisibleEmpty(state: MealsTopBarState?) {
        assertEquals(false, state?.isSearchVisible)
        assertEquals(SEARCH_TERM_NONE, state?.searchTerm)
    }

    @Test
    fun topBarInitialCorrect() = runTest {
        setRepoSuccess()

        initVm()

        viewModel.viewState.map { it?.topBarState }.test {
            val initialState = awaitItem()

            assertTopBarInvisibleEmpty(initialState)

            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    fun searchClick_topBarSearchVisible() = runTest {
        setRepoSuccess()

        initVm()

        viewModel.viewState.map { it?.topBarState }.test {
            val initialState = awaitItem()
            assertTopBarInvisibleEmpty(initialState)

            viewModel.onSearchPressed()

            val newState = awaitItem()
            assertEquals(true, newState?.isSearchVisible)
            assertEquals(SEARCH_TERM_NONE, newState?.searchTerm)

            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    fun searchClick_clearClick_topBarInvisibleClear() = runTest {
        setRepoSuccess()

        initVm()

        viewModel.viewState.map { it?.topBarState }.test {
            val initialState = awaitItem()
            assertTopBarInvisibleEmpty(initialState)

            viewModel.onSearchPressed()

            val newState = awaitItem()
            assertEquals(true, newState?.isSearchVisible)
            assertEquals(SEARCH_TERM_NONE, newState?.searchTerm)

            viewModel.onClearSearchPressed()

            assertTopBarInvisibleEmpty(initialState)

            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    fun searchClick_clearFocus_topBarInvisibleClear() = runTest {
        setRepoSuccess()

        initVm()

        viewModel.viewState.map { it?.topBarState }.test {
            val initialState = awaitItem()
            assertTopBarInvisibleEmpty(initialState)

            viewModel.onSearchPressed()
            viewModel.onSearchFocusChanged(true)

            val newState = awaitItem()
            assertEquals(true, newState?.isSearchVisible)
            assertEquals(SEARCH_TERM_NONE, newState?.searchTerm)

            viewModel.onSearchFocusChanged(false)

            assertTopBarInvisibleEmpty(initialState)

            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    fun searchClick_typeText_clearFocus_topBarVisible() = runTest {
        setRepoSuccess()

        initVm()

        viewModel.viewState.map { it?.topBarState }.test {
            val initialState = awaitItem()
            assertTopBarInvisibleEmpty(initialState)

            viewModel.onSearchPressed()
            viewModel.onSearchFocusChanged(true)

            val visibleState = awaitItem()
            assertEquals(true, visibleState?.isSearchVisible)
            assertEquals(SEARCH_TERM_NONE, visibleState?.searchTerm)

            viewModel.onSearchTermChanged(SEARCH_TERM_SOME)
            viewModel.onSearchFocusChanged(false)

            val typedState = awaitItem()
            assertEquals(true, typedState?.isSearchVisible)
            assertEquals(SEARCH_TERM_SOME, typedState?.searchTerm)

            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    fun onMealPressed_eventCalled() = runTest {
        setRepoSuccess()

        initVm()

        viewModel.eventNavigateDetails.asFlow().test {
            viewModel.onMealPressed(MEAL_NO_TERM.id)

            assertEquals(MEAL_NO_TERM.id, awaitItem())
            val remainingEvents = cancelAndConsumeRemainingEvents()
            assert(remainingEvents.isEmpty()) { "Unexpected events were detected" }
        }
    }

    companion object {
        private const val SEARCH_TERM_NONE = ""
        private val MEAL_NO_TERM = Meal("id", "title", "imageUrl")
        private const val SEARCH_TERM_SOME = "test"
        private val MEAL_SOME_TERM = Meal("id1", "title1", "imageUrl1")
    }
}