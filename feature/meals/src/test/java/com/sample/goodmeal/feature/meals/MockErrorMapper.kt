package com.sample.goodmeal.feature.meals

import com.sample.goodmeal.core.presentation.error.ErrorMapper
import com.samplegoodmeal.core.model.DomainException

class MockErrorMapper : ErrorMapper {
    override fun mapErrorMessage(exception: DomainException) = ERROR_MESSAGE

    companion object {
        const val ERROR_MESSAGE = "error"
    }
}