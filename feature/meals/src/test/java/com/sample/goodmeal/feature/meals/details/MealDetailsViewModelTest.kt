package com.sample.goodmeal.feature.meals.details

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.asFlow
import app.cash.turbine.test
import com.sample.goodmeal.core.presentation.error.ErrorMapper
import com.sample.goodmeal.data.meals.MealsRepository
import com.sample.goodmeal.feature.meals.MockErrorMapper
import com.samplegoodmeal.core.model.DetailedMeal
import com.samplegoodmeal.core.model.NoInternetException
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MealDetailsViewModelTest {
    private val savedStateHandle = SavedStateHandle(
        mapOf(KEY_MEAL_ID to MEAL_ID)
    )
    private val errorMapper: ErrorMapper = MockErrorMapper()
    private lateinit var mealsRepository: MealsRepository

    private lateinit var viewModel: MealDetailsViewModel

    private val testDispatcher = UnconfinedTestDispatcher()

    @get:Rule
    val taskExecutorRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
        mealsRepository = mockk()
    }

    private fun initVm() {
        viewModel = MealDetailsViewModel(savedStateHandle, mealsRepository, errorMapper)
    }

    @After
    fun cleanUp() {
        Dispatchers.resetMain()
    }

    private fun setRepoError() =
        coEvery { mealsRepository.findMealById(MEAL_ID) } returns flow {
            throw NoInternetException()
        }

    private fun setRepoSuccess() =
        coEvery { mealsRepository.findMealById(MEAL_ID) } returns flowOf(DETAILED_MEAL_DOMAIN)

    @Test
    fun repoDoesntEmit_progressState() = runTest {
        coEvery { mealsRepository.findMealById(MEAL_ID) } returns emptyFlow()

        initVm()

        viewModel.viewState.test {
            assertEquals(MealDetailsProgress, awaitItem())
            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    fun repoException_errorState() = runTest {
        setRepoError()

        initVm()

        viewModel.viewState.test {
            val actual = awaitItem()
            assert(actual is MealDetailsError) { "Expected MealDetailsError" }
            val actualErrorMessage = (actual as MealDetailsError).errorMessage
            assertEquals(MockErrorMapper.ERROR_MESSAGE, actualErrorMessage)
            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    fun repoSuccess_contentState() = runTest {
        setRepoSuccess()

        initVm()

        viewModel.viewState.test {
            val actual = awaitItem()
            assert(actual is MealDetailsContent) { "Expected MealDetailsContent" }
            val actualContent = (actual as MealDetailsContent).meal
            assertEquals(DETAILED_MEAL_DOMAIN, actualContent)
            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    fun repoErrorThenSuccess_errorRetryContentState() = runTest {
        setRepoError()

        initVm()

        viewModel.viewState.test {
            val actualError = awaitItem()
            assert(actualError is MealDetailsError) { "Expected MealDetailsError" }

            coEvery { mealsRepository.findMealById(MEAL_ID) } returns flowOf(DETAILED_MEAL_DOMAIN)
            viewModel.onRetryLoading()

            val actualContent = awaitItem()
            assert(actualContent is MealDetailsContent) { "Expected MealDetailsContent" }
            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    fun onOpenVideoPressed_repoSuccess_eventCalled() = runTest {
        setRepoSuccess()

        initVm()

        viewModel.eventOpenVideo.asFlow().test {
            viewModel.onOpenVideoPressed()
            assertEquals(VIDEO_URL, awaitItem())
            val remainingEvents = cancelAndConsumeRemainingEvents()
            assert(remainingEvents.isEmpty()) { "Unexpected events were detected" }
        }
    }

    @Test
    fun onBackPressed_repoSuccess_eventCalled() = runTest {
        setRepoSuccess()

        initVm()

        viewModel.eventNavigateBack.asFlow().test {
            viewModel.onBackPressed()

            awaitItem()
            val remainingEvents = cancelAndConsumeRemainingEvents()
            assert(remainingEvents.isEmpty()) { "Unexpected events were detected" }
        }
    }

    companion object {
        private const val KEY_MEAL_ID = "mealId"
        private const val MEAL_ID = "id"
        private const val VIDEO_URL = "videoUrl"
        private val DETAILED_MEAL_DOMAIN = DetailedMeal(
            "id", "title", "imageUrl", VIDEO_URL, "instructions", emptyList()
        )
    }
}