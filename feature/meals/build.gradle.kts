plugins {
    id("goodmeal.android.library")
    id("goodmeal.android.library.compose")
    id("goodmeal.hilt")
    with(libs.plugins) {
        alias(navigation)
    }
}

android {
    namespace = "com.sample.goodmeal.feature.meals"
}

dependencies {
    implementation(projects.data.meals)
    implementation(projects.domain.meals)
    implementation(projects.core.model)
    implementation(projects.core.compose)
    implementation(projects.core.presentation)
    with(libs) {
        implementation(androidCore)
        implementation(bundles.navigation)
        implementation(bundles.lifecycle)
        implementation(bundles.compose)
        testImplementation(bundles.unitTesting)
    }
}