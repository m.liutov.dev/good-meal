plugins {
    id("goodmeal.android.library")
    id("goodmeal.hilt")
}

android {
    namespace = "com.sample.goodmeal.data.user"
}

dependencies {
    implementation(projects.core.model)
}