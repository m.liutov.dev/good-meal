package com.sample.goodmeal.data.user

import com.samplegoodmeal.core.model.User
import javax.inject.Inject

/**
 * Dummy repository, present in project only to have a second repository, to be able to add
 * a use case.
 */
class UserRepository @Inject constructor() {

    fun getUser() = User(User.SortingPreference.ALPHABETIC)

}