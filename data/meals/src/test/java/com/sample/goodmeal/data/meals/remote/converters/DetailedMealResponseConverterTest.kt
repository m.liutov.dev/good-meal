package com.sample.goodmeal.data.meals.remote.converters

import com.sample.goodmeal.data.meals.remote.response.DetailedMealResponse
import com.samplegoodmeal.core.model.DetailedMeal
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class DetailedMealResponseConverterTest {
    private lateinit var converter: DetailedMealResponseConverter

    @Before
    fun setup() {
        converter = DetailedMealResponseConverter()
    }

    @Test
    fun fromResponse_noTags_correctMapping() {
        val actual = converter.fromResponse(MEAL_RESPONSE_NO_TAGS)

        assertEquals(MEAL_DOMAIN_NO_TAGS, actual)
    }

    @Test
    fun fromResponse_withTags_correctMapping() {
        val actual = converter.fromResponse(MEAL_RESPONSE_TAGS)

        assertEquals(MEAL_DOMAIN_TAGS, actual)
    }

    companion object {
        private val MEAL_RESPONSE_NO_TAGS = DetailedMealResponse(
            "id", "instructions", "title", "imageUrl", "videoUrl", null
        )
        private val MEAL_DOMAIN_NO_TAGS = DetailedMeal(
            "id", "title", "imageUrl", "videoUrl", "instructions", emptyList()
        )
        private val MEAL_RESPONSE_TAGS = MEAL_RESPONSE_NO_TAGS.copy(
            tags = listOf("tag1", "tag2")
        )
        private val MEAL_DOMAIN_TAGS = MEAL_DOMAIN_NO_TAGS.copy(
            tags = listOf("tag1", "tag2")
        )
    }
}