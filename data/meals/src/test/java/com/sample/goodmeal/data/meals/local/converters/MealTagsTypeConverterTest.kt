package com.sample.goodmeal.data.meals.local.converters

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class MealTagsTypeConverterTest {
    private lateinit var converter: DetailedMealEntityConverter

    @Before
    fun setup() {
        converter = DetailedMealEntityConverter()
    }

    @Test
    fun serialize_correctMapping() {
        val actual = MealTagsTypeConverter.to(TAGS_OBJECT)

        assertEquals(TAGS_SERIALIZED, actual)
    }

    @Test
    fun deserialize_correctMapping() {
        val actual = MealTagsTypeConverter.from(TAGS_SERIALIZED)

        assertEquals(TAGS_OBJECT, actual)
    }

    companion object {
        private val TAGS_OBJECT = listOf("tag1", "tag2", "tag3", "tag4")
        private const val TAGS_SERIALIZED = "tag1,tag2,tag3,tag4"
    }
}