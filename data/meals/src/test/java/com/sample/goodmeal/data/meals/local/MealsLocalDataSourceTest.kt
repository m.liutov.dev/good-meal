package com.sample.goodmeal.data.meals.local

import app.cash.turbine.test
import com.sample.goodmeal.data.meals.local.converters.DetailedMealEntityConverter
import com.sample.goodmeal.data.meals.local.db.DetailedMealsDao
import com.sample.goodmeal.data.meals.local.entity.DetailedMealEntity
import com.samplegoodmeal.core.model.DetailedMeal
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class MealsLocalDataSourceTest {

    private lateinit var mealsDao: DetailedMealsDao
    private lateinit var converter: DetailedMealEntityConverter

    private lateinit var dataSource: MealsLocalDataSource

    @Before
    fun setup() {
        mealsDao = mockk(relaxUnitFun = true)
        converter = mockk()

        coEvery { mealsDao.getById(MEAL_ENTITY.id) } returns MEAL_ENTITY
        coEvery { mealsDao.getFlowById(MEAL_ENTITY.id) } returns flowOf(MEAL_ENTITY)

        every { converter.toEntity(MEAL_DOMAIN) } returns MEAL_ENTITY
        every { converter.fromEntity(MEAL_ENTITY) } returns MEAL_DOMAIN

        dataSource = MealsLocalDataSource(mealsDao, converter)
    }

    @Test
    fun save_convertedAndInsertCalled() = runTest {
        dataSource.save(MEAL_DOMAIN)

        coVerify(exactly = 1) { mealsDao.insert(MEAL_ENTITY) }
    }

    @Test
    fun getById_convertedAndReturned() = runTest {
        val actual = dataSource.getById(MEAL_DOMAIN.id)

        assertEquals(MEAL_DOMAIN, actual)
    }

    @Test
    fun getFlowById_convertedAndReturned() = runTest {
        dataSource.getFlowById(MEAL_DOMAIN.id).test {
            assertEquals(MEAL_DOMAIN, awaitItem())
            cancelAndIgnoreRemainingEvents()
        }
    }

    companion object {
        private val MEAL_DOMAIN = DetailedMeal(
            "id", "title", "imageUrl", "videoUrl", "instructions", listOf("tag1", "tag2")
        )
        private val MEAL_ENTITY = DetailedMealEntity(
            "id", "title", "imageUrl", "videoUrl", "instructions", listOf("tag1", "tag2")
        )
    }
}