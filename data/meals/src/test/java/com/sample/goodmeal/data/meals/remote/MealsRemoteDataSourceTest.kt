package com.sample.goodmeal.data.meals.remote

import com.sample.goodmeal.data.meals.remote.api.MealsService
import com.sample.goodmeal.data.meals.remote.converters.DetailedMealResponseConverter
import com.sample.goodmeal.data.meals.remote.converters.MealResponseConverter
import com.sample.goodmeal.data.meals.remote.response.DetailedMealResponse
import com.sample.goodmeal.data.meals.remote.response.MealResponse
import com.sample.goodmeal.data.meals.remote.response.MealsResponseWrapper
import com.samplegoodmeal.core.model.DetailedMeal
import com.samplegoodmeal.core.model.Meal
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class MealsRemoteDataSourceTest {

    private lateinit var mealsService: MealsService
    private lateinit var detailedMealConverter: DetailedMealResponseConverter
    private lateinit var mealConverter: MealResponseConverter

    private lateinit var dataSource: MealsRemoteDataSource

    @Before
    fun setup() {
        mealsService = mockk()
        detailedMealConverter = mockk()
        mealConverter = mockk()

        every { detailedMealConverter.fromResponse(DETAILED_MEAL_RESPONSE) } returns DETAILED_MEAL_DOMAIN
        every { mealConverter.fromResponse(MEAL_RESPONSE) } returns MEAL_DOMAIN
        coEvery { mealsService.searchMeals(SEARCH_TERM) } returns MealsResponseWrapper(
            listOf(
                MEAL_RESPONSE
            )
        )
        coEvery { mealsService.findMealById(DETAILED_MEAL_DOMAIN.id) } returns MealsResponseWrapper(
            listOf(
                DETAILED_MEAL_RESPONSE
            )
        )

        dataSource = MealsRemoteDataSource(mealsService, detailedMealConverter, mealConverter)
    }

    @Test
    fun searchMeals_convertedAndReturned() = runTest {
        val actual = dataSource.searchMeals(SEARCH_TERM)

        assertEquals(listOf(MEAL_DOMAIN), actual)
    }

    @Test
    fun findMealById_convertedAndReturned() = runTest {
        val actual = dataSource.findMealById(DETAILED_MEAL_DOMAIN.id)

        assertEquals(DETAILED_MEAL_DOMAIN, actual)
    }

    companion object {
        private const val SEARCH_TERM = "term"
        private val DETAILED_MEAL_RESPONSE = DetailedMealResponse(
            "id", "instructions", "title", "imageUrl", "videoUrl", null
        )
        private val DETAILED_MEAL_DOMAIN = DetailedMeal(
            "id", "title", "imageUrl", "videoUrl", "instructions", emptyList()
        )
        private val MEAL_RESPONSE = MealResponse(
            "id", "title", "imageUrl"
        )
        private val MEAL_DOMAIN = Meal(
            "id", "title", "imageUrl"
        )
    }
}