package com.sample.goodmeal.data.meals

import app.cash.turbine.test
import com.sample.goodmeal.core.common.DispatchersProvider
import com.sample.goodmeal.data.meals.local.MealsLocalDataSource
import com.sample.goodmeal.data.meals.remote.MealsRemoteDataSource
import com.samplegoodmeal.core.model.DetailedMeal
import com.samplegoodmeal.core.model.Meal
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class MealsRepositoryTest {

    private val testDispatcher = UnconfinedTestDispatcher()

    private lateinit var remoteDataSource: MealsRemoteDataSource
    private lateinit var localDataSource: MealsLocalDataSource
    private val dispatchersProvider =
        DispatchersProvider(testDispatcher, testDispatcher, testDispatcher)

    private lateinit var repository: MealsRepository

    @Before
    fun setup() {
        remoteDataSource = mockk()
        localDataSource = mockk(relaxUnitFun = true)
        coEvery { localDataSource.getFlowById(DETAILED_MEAL_DOMAIN.id) } returns flowOf(
            DETAILED_MEAL_DOMAIN
        )

        repository = MealsRepository(remoteDataSource, localDataSource, dispatchersProvider)
    }

    @Test
    fun searchMeals_withTerm_remoteSourceCalled() = runTest {
        executeSearchTest(SEARCH_TERM)
    }

    @Test
    fun searchMeals_noTerm_remoteSourceCalled() = runTest {
        executeSearchTest("")
    }

    private suspend fun executeSearchTest(term: String) {
        coEvery { remoteDataSource.searchMeals(term) } returns listOf(MEAL_DOMAIN)

        val actual = repository.searchMeals(term)

        assertEquals(listOf(MEAL_DOMAIN), actual)
    }

    @Test
    fun findMealById_cached_returnedFromLocal() = runTest {
        coEvery { localDataSource.getById(DETAILED_MEAL_DOMAIN.id) } returns DETAILED_MEAL_DOMAIN

        repository.findMealById(DETAILED_MEAL_DOMAIN.id).test {
            assertEquals(DETAILED_MEAL_DOMAIN, awaitItem())
            awaitComplete()
        }
    }

    @Test
    fun findMealById_notCached_loadedAndReturnedFromLocal() = runTest {
        coEvery { localDataSource.getById(DETAILED_MEAL_DOMAIN.id) } returns null
        coEvery { remoteDataSource.findMealById(DETAILED_MEAL_DOMAIN.id) } returns DETAILED_MEAL_DOMAIN

        repository.findMealById(DETAILED_MEAL_DOMAIN.id).test {
            assertEquals(null, awaitItem())
            assertEquals(DETAILED_MEAL_DOMAIN, awaitItem())
            awaitComplete()
        }
        coVerify(exactly = 1) { localDataSource.save(DETAILED_MEAL_DOMAIN) }
    }

    companion object {
        private const val SEARCH_TERM = "term"
        private val DETAILED_MEAL_DOMAIN = DetailedMeal(
            "id", "title", "imageUrl", "videoUrl", "instructions", emptyList()
        )
        private val MEAL_DOMAIN = Meal(
            "id", "title", "imageUrl"
        )
    }
}