package com.sample.goodmeal.data.meals.remote.converters

import com.sample.goodmeal.data.meals.remote.response.MealResponse
import com.samplegoodmeal.core.model.Meal
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class MealResponseConverterTest {
    private lateinit var converter: MealResponseConverter

    @Before
    fun setup() {
        converter = MealResponseConverter()
    }

    @Test
    fun fromResponse_correctMapping() {
        val actual = converter.fromResponse(MEAL_RESPONSE)

        assertEquals(MEAL_DOMAIN, actual)
    }

    companion object {
        private val MEAL_RESPONSE = MealResponse(
            "id", "title", "imageUrl"
        )
        private val MEAL_DOMAIN = Meal(
            "id", "title", "imageUrl"
        )
    }
}