package com.sample.goodmeal.data.meals.local.converters

import com.sample.goodmeal.data.meals.local.entity.DetailedMealEntity
import com.samplegoodmeal.core.model.DetailedMeal
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class DetailedMealEntityConverterTest {

    private lateinit var converter: DetailedMealEntityConverter

    @Before
    fun setup() {
        converter = DetailedMealEntityConverter()
    }

    @Test
    fun toEntity_correctMapping() {
        val actual = converter.toEntity(MEAL_DOMAIN)

        assertEquals(MEAL_ENTITY, actual)
    }

    @Test
    fun fromEntity_correctMapping() {
        val actual = converter.fromEntity(MEAL_ENTITY)

        assertEquals(MEAL_DOMAIN, actual)
    }

    companion object {
        private val MEAL_DOMAIN = DetailedMeal(
            "id", "title", "imageUrl", "videoUrl", "instructions", listOf("tag1", "tag2")
        )
        private val MEAL_ENTITY = DetailedMealEntity(
            "id", "title", "imageUrl", "videoUrl", "instructions", listOf("tag1", "tag2")
        )
    }
}