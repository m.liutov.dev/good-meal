package com.sample.goodmeal.data.meals.local.db

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import app.cash.turbine.test
import com.sample.goodmeal.data.meals.local.entity.DetailedMealEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DetailedMealsDaoTest {

    private lateinit var mealsDao: DetailedMealsDao
    private lateinit var db: MealsDatabase

    private val testDispatcher = UnconfinedTestDispatcher()

    @Before
    fun createDb() {
        Dispatchers.setMain(testDispatcher)
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, MealsDatabase::class.java)
            .build()
        mealsDao = db.detailedMealsDao()
    }

    @After
    fun closeDb() {
        db.close()
        Dispatchers.resetMain()
    }

    @Test
    @Throws(Exception::class)
    fun readWrite_correctValue() = runTest {
        mealsDao.insert(MEAL_ENTITY)

        val actual = mealsDao.getById(MEAL_ENTITY.id)

        assertEquals(MEAL_ENTITY, actual)
    }

    @Test
    @Throws(Exception::class)
    fun readWrite_correctFlow() = runTest {
        mealsDao.getFlowById(MEAL_ENTITY.id).test {
            mealsDao.insert(MEAL_ENTITY)
            assertEquals(MEAL_ENTITY, awaitItem())
            cancelAndIgnoreRemainingEvents()
        }
    }

    companion object {
        private val MEAL_ENTITY = DetailedMealEntity(
            "id", "title", "imageUrl", "videoUrl", "instructions", listOf("tag1", "tag1")
        )
    }
}