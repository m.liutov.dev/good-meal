package com.sample.goodmeal.data.meals.remote.converters

import com.sample.goodmeal.data.meals.remote.response.MealResponse
import com.samplegoodmeal.core.model.Meal
import javax.inject.Inject

internal class MealResponseConverter @Inject constructor() {
    fun fromResponse(response: MealResponse): Meal = with(response) {
        Meal(id, meal, thumbnailUrl)
    }
}