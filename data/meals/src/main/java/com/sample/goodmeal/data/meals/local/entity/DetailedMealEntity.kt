package com.sample.goodmeal.data.meals.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = DetailedMealEntity.TABLE_NAME)
internal data class DetailedMealEntity(
    @PrimaryKey
    val id: String,
    val title: String,
    val imageUrl: String,
    val videoUrl: String,
    val instructions: String,
    val mealTags: List<String>,
) {

    companion object {
        const val TABLE_NAME = "detailed_meal"
    }
}