package com.sample.goodmeal.data.meals.remote.response

import com.sample.goodmeal.data.meals.remote.serializers.MealTagsSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class DetailedMealResponse(
    @SerialName("idMeal")
    val id: String,
    @SerialName("strInstructions")
    val instructions: String,
    @SerialName("strMeal")
    val title: String,
    @SerialName("strMealThumb")
    val imageUrl: String,
    @SerialName("strYoutube")
    val videoUrl: String,
    @SerialName("strTags")
    @Serializable(MealTagsSerializer::class)
    val tags: List<String>?,
)