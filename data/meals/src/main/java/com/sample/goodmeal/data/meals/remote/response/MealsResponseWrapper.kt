package com.sample.goodmeal.data.meals.remote.response

import kotlinx.serialization.Serializable


@Serializable
internal data class MealsResponseWrapper<T>(
    val meals: List<T>,
)






