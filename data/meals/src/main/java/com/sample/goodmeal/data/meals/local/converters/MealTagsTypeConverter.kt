package com.sample.goodmeal.data.meals.local.converters

import androidx.room.TypeConverter

internal object MealTagsTypeConverter {

    private const val SEPARATOR = ","

    @TypeConverter
    fun from(value: String): List<String> = value.split(SEPARATOR)

    @TypeConverter
    fun to(tags: List<String>): String = tags.joinToString(SEPARATOR)

}