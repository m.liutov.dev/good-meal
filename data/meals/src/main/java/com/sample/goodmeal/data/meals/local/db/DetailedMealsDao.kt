package com.sample.goodmeal.data.meals.local.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sample.goodmeal.data.meals.local.entity.DetailedMealEntity
import kotlinx.coroutines.flow.Flow

@Dao
internal interface DetailedMealsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(detailedMeal: DetailedMealEntity)

    @Query("SELECT * FROM ${DetailedMealEntity.TABLE_NAME} WHERE id=:id")
    suspend fun getById(id: String): DetailedMealEntity?

    @Query("SELECT * FROM ${DetailedMealEntity.TABLE_NAME} WHERE id=:id")
    fun getFlowById(id: String): Flow<DetailedMealEntity?>
}