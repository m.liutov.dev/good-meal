package com.sample.goodmeal.data.meals

import android.content.Context
import androidx.room.Room
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.sample.goodmeal.core.common.DispatchersProvider
import com.sample.goodmeal.core.network.NetworkModule
import com.sample.goodmeal.core.network.exception.ErrorHandlingAdapterFactory
import com.sample.goodmeal.data.meals.local.MealsLocalDataSource
import com.sample.goodmeal.data.meals.local.converters.DetailedMealEntityConverter
import com.sample.goodmeal.data.meals.local.db.DetailedMealsDao
import com.sample.goodmeal.data.meals.local.db.MealsDatabase
import com.sample.goodmeal.data.meals.remote.MealsRemoteDataSource
import com.sample.goodmeal.data.meals.remote.api.MealsService
import com.sample.goodmeal.data.meals.remote.api.MealsUrlProvider
import com.sample.goodmeal.data.meals.remote.converters.DetailedMealResponseConverter
import com.sample.goodmeal.data.meals.remote.converters.MealResponseConverter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MealsDataModule {

    @Provides
    @Meals
    @Singleton
    internal fun provideOkHttpClient(@NetworkModule.Base baseClient: OkHttpClient): OkHttpClient =
        baseClient.newBuilder().build()

    @Provides
    @Meals
    @Singleton
    internal fun provideRetrofit(
        @Meals httpClient: OkHttpClient,
        @NetworkModule.Base json: Json,
    ): Retrofit =
        Retrofit.Builder()
            .baseUrl(MealsUrlProvider.BASE_URL)
            .client(httpClient)
            .addCallAdapterFactory(ErrorHandlingAdapterFactory())
            .addConverterFactory(
                @OptIn(ExperimentalSerializationApi::class)
                json.asConverterFactory("application/json".toMediaType()),
            )
            .build()

    @Provides
    @Singleton
    internal fun provideMealsService(@Meals retrofit: Retrofit): MealsService =
        retrofit.create(MealsService::class.java)

    @Provides
    @Singleton
    internal fun provideMealsDatabase(@ApplicationContext context: Context): MealsDatabase =
        Room.databaseBuilder(context, MealsDatabase::class.java, MealsDatabase.DB_NAME)
            .build()

    @Provides
    @Singleton
    internal fun provideDetailedMealsDao(mealsDatabase: MealsDatabase): DetailedMealsDao =
        mealsDatabase.detailedMealsDao()

    @Provides
    @Singleton
    internal fun provideRemoteDataSource(
        mealsService: MealsService,
        detailedMealConverter: DetailedMealResponseConverter,
        mealConverter: MealResponseConverter,
    ): MealsRemoteDataSource =
        MealsRemoteDataSource(mealsService, detailedMealConverter, mealConverter)

    @Provides
    @Singleton
    internal fun provideLocalDataSource(
        mealsDao: DetailedMealsDao,
        converter: DetailedMealEntityConverter,
    ): MealsLocalDataSource = MealsLocalDataSource(mealsDao, converter)

    @Provides
    @Singleton
    internal fun provideMealsRepository(
        mealsRemoteDataSource: MealsRemoteDataSource,
        mealsLocalDataSource: MealsLocalDataSource,
        dispatchersProvider: DispatchersProvider,
    ): MealsRepository = MealsRepository(
        mealsRemoteDataSource, mealsLocalDataSource, dispatchersProvider
    )

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class Meals
}