package com.sample.goodmeal.data.meals.remote.serializers

import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

internal object MealTagsSerializer : KSerializer<List<String>?> {

    private const val TAGS_SEPARATOR = ","

    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("Tags", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: List<String>?) {
        encoder.encodeString(value?.joinToString(separator = TAGS_SEPARATOR).orEmpty())
    }

    override fun deserialize(decoder: Decoder): List<String> {
        return decoder.decodeString().split(TAGS_SEPARATOR)
    }

}