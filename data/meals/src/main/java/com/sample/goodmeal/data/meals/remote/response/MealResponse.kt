package com.sample.goodmeal.data.meals.remote.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MealResponse(
    @SerialName("idMeal")
    val id: String,
    @SerialName("strMeal")
    val meal: String,
    @SerialName("strMealThumb")
    val thumbnailUrl: String,
)