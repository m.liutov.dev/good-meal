package com.sample.goodmeal.data.meals.local.converters

import com.sample.goodmeal.data.meals.local.entity.DetailedMealEntity
import com.samplegoodmeal.core.model.DetailedMeal
import javax.inject.Inject

internal class DetailedMealEntityConverter @Inject constructor() {
    fun toEntity(detailedMeal: DetailedMeal): DetailedMealEntity = with(detailedMeal) {
        DetailedMealEntity(id, title, imageUrl, videoUrl, instructions, tags)
    }

    fun fromEntity(entity: DetailedMealEntity): DetailedMeal = with(entity) {
        DetailedMeal(id, title, imageUrl, videoUrl, instructions, mealTags)
    }
}