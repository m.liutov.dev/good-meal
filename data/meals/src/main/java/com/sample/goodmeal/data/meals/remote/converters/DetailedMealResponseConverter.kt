package com.sample.goodmeal.data.meals.remote.converters

import com.sample.goodmeal.data.meals.remote.response.DetailedMealResponse
import com.samplegoodmeal.core.model.DetailedMeal
import javax.inject.Inject

internal class DetailedMealResponseConverter @Inject constructor() {
    fun fromResponse(response: DetailedMealResponse): DetailedMeal = with(response) {
        DetailedMeal(id, title, imageUrl, videoUrl, instructions, tags.orEmpty())
    }
}