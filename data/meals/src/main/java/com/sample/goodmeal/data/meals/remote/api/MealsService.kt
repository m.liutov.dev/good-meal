package com.sample.goodmeal.data.meals.remote.api

import com.sample.goodmeal.data.meals.remote.response.DetailedMealResponse
import com.sample.goodmeal.data.meals.remote.response.MealResponse
import com.sample.goodmeal.data.meals.remote.response.MealsResponseWrapper
import retrofit2.http.GET
import retrofit2.http.Query

internal interface MealsService {

    @GET("search.php")
    suspend fun searchMeals(
        @Query(QUERY_SEARCH_TERM) searchTerm: String,
    ): MealsResponseWrapper<MealResponse>

    @GET("lookup.php")
    suspend fun findMealById(
        @Query(QUERY_ID) id: String,
    ): MealsResponseWrapper<DetailedMealResponse>

    companion object {
        private const val QUERY_SEARCH_TERM = "s"
        private const val QUERY_ID = "i"
    }
}