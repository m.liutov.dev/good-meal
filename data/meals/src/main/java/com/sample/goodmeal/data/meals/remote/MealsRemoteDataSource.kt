package com.sample.goodmeal.data.meals.remote

import com.sample.goodmeal.data.meals.remote.api.MealsService
import com.sample.goodmeal.data.meals.remote.converters.DetailedMealResponseConverter
import com.sample.goodmeal.data.meals.remote.converters.MealResponseConverter
import com.samplegoodmeal.core.model.DetailedMeal
import com.samplegoodmeal.core.model.Meal

internal class MealsRemoteDataSource(
    private val mealsService: MealsService,
    private val detailedMealConverter: DetailedMealResponseConverter,
    private val mealConverter: MealResponseConverter,
) {

    suspend fun searchMeals(searchTerm: String): List<Meal> =
        mealsService.searchMeals(searchTerm).meals.map { mealConverter.fromResponse(it) }

    suspend fun findMealById(id: String): DetailedMeal =
        mealsService.findMealById(id).meals.first().let { detailedMealConverter.fromResponse(it) }

}