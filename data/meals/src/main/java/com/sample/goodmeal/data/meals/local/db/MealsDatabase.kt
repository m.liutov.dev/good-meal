package com.sample.goodmeal.data.meals.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.sample.goodmeal.data.meals.local.converters.MealTagsTypeConverter
import com.sample.goodmeal.data.meals.local.entity.DetailedMealEntity

@Database(entities = [DetailedMealEntity::class], version = 1, exportSchema = true)
@TypeConverters(value = [MealTagsTypeConverter::class])
internal abstract class MealsDatabase : RoomDatabase() {
    abstract fun detailedMealsDao(): DetailedMealsDao

    companion object {
        const val DB_NAME = "meals"
    }
}