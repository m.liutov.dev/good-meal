package com.sample.goodmeal.data.meals.local

import com.sample.goodmeal.data.meals.local.converters.DetailedMealEntityConverter
import com.sample.goodmeal.data.meals.local.db.DetailedMealsDao
import com.samplegoodmeal.core.model.DetailedMeal
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

internal class MealsLocalDataSource(
    private val mealsDao: DetailedMealsDao,
    private val converter: DetailedMealEntityConverter,
) {

    suspend fun save(detailedMeal: DetailedMeal) =
        mealsDao.insert(converter.toEntity(detailedMeal))

    suspend fun getById(id: String): DetailedMeal? =
        mealsDao.getById(id)?.let { converter.fromEntity(it) }

    fun getFlowById(id: String): Flow<DetailedMeal?> = mealsDao.getFlowById(id).map { entity ->
        entity?.let { converter.fromEntity(it) }
    }

}