package com.sample.goodmeal.data.meals.remote.api

internal object MealsUrlProvider {
    internal const val BASE_URL = "https://www.themealdb.com/api/json/v1/1/"
}