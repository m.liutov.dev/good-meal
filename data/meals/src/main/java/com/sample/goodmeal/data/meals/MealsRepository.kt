package com.sample.goodmeal.data.meals

import com.sample.goodmeal.core.common.DispatchersProvider
import com.sample.goodmeal.data.meals.local.MealsLocalDataSource
import com.sample.goodmeal.data.meals.remote.MealsRemoteDataSource
import com.samplegoodmeal.core.model.DetailedMeal
import com.samplegoodmeal.core.model.Meal
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext

class MealsRepository internal constructor(
    private val remoteDataSource: MealsRemoteDataSource,
    private val localDataSource: MealsLocalDataSource,
    private val dispatchersProvider: DispatchersProvider,
) {

    suspend fun searchMeals(searchTerm: String?): List<Meal> = withContext(dispatchersProvider.io) {
        remoteDataSource.searchMeals(searchTerm.orEmpty())
    }

    fun findMealById(id: String): Flow<DetailedMeal?> = flow {
        if (localDataSource.getById(id) == null) {
            emit(null)
            val response = remoteDataSource.findMealById(id)
            localDataSource.save(response)
        }
        emitAll(localDataSource.getFlowById(id))
    }.flowOn(dispatchersProvider.io)

}