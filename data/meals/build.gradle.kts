plugins {
    id("goodmeal.android.library")
    id("goodmeal.android.library.androidtest")
    id("goodmeal.hilt")
    id("goodmeal.room")
    with(libs.plugins) {
        alias(kotlin.kapt)
        alias(kotlin.serialization)
    }
}

android {
    namespace = "com.sample.goodmeal.data.meals"
}

dependencies {
    implementation(projects.core.model)
    implementation(projects.core.network)
    implementation(projects.core.common)

    with(libs) {
        implementation(androidCore)
        implementation(bundles.network)
        testImplementation(bundles.unitTesting)
    }
}