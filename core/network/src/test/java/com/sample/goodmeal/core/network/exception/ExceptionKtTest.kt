package com.sample.goodmeal.core.network.exception

import com.samplegoodmeal.core.model.ApiException
import com.samplegoodmeal.core.model.NoInternetException
import com.samplegoodmeal.core.model.UnexpectedException
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert.assertEquals
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

internal class ExceptionKtTest {

    @Test
    fun ioException_correctMapping() {
        val actual = mapToDomainException(IOException())

        assert(actual is NoInternetException) { "Wrong exception type. Expected NoInternetException" }
    }

    @Test
    fun httpException_correctMapping() {
        val expectedCode = 412
        val testResponse = Response.error<Unit>(expectedCode, "".toResponseBody())

        val actual = mapToDomainException(HttpException(testResponse))

        assert(actual is ApiException) { "Wrong exception type. Expected ApiException" }
        assertEquals(expectedCode.toString(), (actual as ApiException).statusCode)
    }

    @Test
    fun otherException_correctMapping() {
        val actual = mapToDomainException(IllegalArgumentException())

        assert(actual is UnexpectedException) { "Wrong exception type. Expected UnexpectedException" }
    }
}