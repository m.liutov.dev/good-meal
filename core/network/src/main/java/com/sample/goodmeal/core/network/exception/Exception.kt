package com.sample.goodmeal.core.network.exception

import com.samplegoodmeal.core.model.ApiException
import com.samplegoodmeal.core.model.DomainException
import com.samplegoodmeal.core.model.NoInternetException
import com.samplegoodmeal.core.model.UnexpectedException
import retrofit2.HttpException
import java.io.IOException

fun mapToDomainException(
    remoteException: Throwable,
): DomainException {
    return when (remoteException) {
        is IOException -> NoInternetException()
        is HttpException -> ApiException(remoteException.code().toString())
        else -> UnexpectedException(remoteException)
    }
}