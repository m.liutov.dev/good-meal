package com.sample.goodmeal.core.network

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Qualifier
import javax.inject.Singleton

/**
 * Module for providing shared network dependencies
 */
@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    /**
     * Provides a base version of [OkHttpClient], upon which all specific ones should be built in
     * order to optimize thread and memory usage.
     */
    @Singleton
    @Base
    @Provides
    fun provideBaseOkHttp(): OkHttpClient = OkHttpClient.Builder().apply {
        if (BuildConfig.DEBUG) {
            addInterceptor(
                HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }
            )
        }
    }.build()

    /**
     * Provides a base [Json] instance with basic configuration for working with network
     */
    @Singleton
    @Base
    @Provides
    fun provideJson(): Json = Json {
        ignoreUnknownKeys = true
    }

    /**
     * Represents a base version of dependency, upon which a specific dependency should be built
     */
    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class Base
}