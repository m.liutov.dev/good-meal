package com.sample.goodmeal.core.network.exception

import retrofit2.Call
import retrofit2.CallAdapter

internal class ErrorHandlingAdapter(
    private val delegateAdapter: CallAdapter<Any, Call<*>>,
) : CallAdapter<Any, Call<*>> by delegateAdapter {

    override fun adapt(call: Call<Any>): Call<*> {
        return delegateAdapter.adapt(ErrorHandlingCall(call))
    }
}