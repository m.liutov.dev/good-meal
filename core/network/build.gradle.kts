plugins {
    id("goodmeal.android.library")
    id("goodmeal.hilt")
    with(libs.plugins) {
        alias(kotlin.kapt)
        alias(kotlin.serialization)
    }
}

android {
    namespace = "com.sample.goodmeal.core.network"
}

dependencies {
    implementation(projects.core.model)
    with(libs) {
        implementation(bundles.network)
        testImplementation(bundles.unitTesting)
    }
}