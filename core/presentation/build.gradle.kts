plugins {
    id("goodmeal.android.library")
    id("goodmeal.hilt")
}

android {
    namespace = "com.sample.goodmeal.core.presentation"
}

dependencies {
    implementation(projects.core.model)
    with(libs) {
        implementation(bundles.lifecycle)
        testImplementation(bundles.unitTesting)
    }
}