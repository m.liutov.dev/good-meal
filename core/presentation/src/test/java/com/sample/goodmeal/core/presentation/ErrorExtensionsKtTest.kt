package com.sample.goodmeal.core.presentation

import com.sample.goodmeal.core.presentation.error.catchDomainException
import com.samplegoodmeal.core.model.DomainException
import com.samplegoodmeal.core.model.NoInternetException
import com.samplegoodmeal.core.model.UnexpectedException
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Test

class ErrorExtensionsKtTest {

    @Test
    fun domainException_exceptionPreserved() = runTest {
        val testFlow = flow<DomainException> {
            throw NoInternetException()
        }.catchDomainException {
            emit(it)
        }

        val actual = testFlow.first()

        assert(actual is NoInternetException) { "Wrong exception type. Expected NoInternetException" }
    }

    @Test
    fun otherException_exceptionRemapped() = runTest {
        val testException = IllegalArgumentException()
        val testFlow = flow<DomainException> {
            throw testException
        }.catchDomainException {
            emit(it)
        }

        val actual = testFlow.first()

        assert(actual is UnexpectedException) { "Wrong exception type. Expected UnexpectedException" }
        assertEquals(testException, actual.cause)
    }

}