package com.sample.goodmeal.core.presentation.event

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

fun <T> MutableLiveData<T>.immutable(): LiveData<T> = this