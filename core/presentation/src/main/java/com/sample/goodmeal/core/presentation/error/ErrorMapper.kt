package com.sample.goodmeal.core.presentation.error

import com.samplegoodmeal.core.model.DomainException

/**
 * Mapper for getting error messages to display from domain exceptions.
 */
interface ErrorMapper {
    fun mapErrorMessage(exception: DomainException): String
}