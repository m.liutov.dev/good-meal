package com.sample.goodmeal.core.presentation.error

import com.samplegoodmeal.core.model.DomainException
import com.samplegoodmeal.core.model.UnexpectedException
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.catch

fun <T> Flow<T>.catchDomainException(action: suspend FlowCollector<T>.(DomainException) -> Unit) =
    catch {
        action(it as? DomainException ?: UnexpectedException(it))
    }