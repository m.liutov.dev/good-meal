package com.sample.goodmeal.core.presentation

import com.sample.goodmeal.core.presentation.error.BasicErrorMapper
import com.sample.goodmeal.core.presentation.error.ErrorMapper
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class PresentationModule {
    @Binds
    abstract fun bindErrorMapper(basicErrorMapper: BasicErrorMapper): ErrorMapper
}