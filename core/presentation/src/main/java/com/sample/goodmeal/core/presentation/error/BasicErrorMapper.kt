package com.sample.goodmeal.core.presentation.error

import android.content.Context
import com.sample.goodmeal.core.presentation.R
import com.samplegoodmeal.core.model.ApiException
import com.samplegoodmeal.core.model.DomainException
import com.samplegoodmeal.core.model.NoInternetException
import com.samplegoodmeal.core.model.UnexpectedException
import dagger.Reusable
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

@Reusable
class BasicErrorMapper @Inject constructor(@ApplicationContext private val context: Context) :
    ErrorMapper {

    override fun mapErrorMessage(exception: DomainException): String =
        when (exception) {
            is ApiException -> R.string.generic_error_api
            is NoInternetException -> R.string.generic_error_network
            is UnexpectedException -> R.string.generic_error_unknown
        }.let { context.getString(it) }

}