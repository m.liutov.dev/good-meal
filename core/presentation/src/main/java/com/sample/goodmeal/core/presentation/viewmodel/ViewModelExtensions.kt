package com.sample.goodmeal.core.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.stateIn

/**
 * States a [Flow] in the [viewModelScope], using [SharingStarted.WhileSubscribed] to avoid
 * restarting flow when it's not necessary.
 */
context(ViewModel)
fun <T> Flow<T>.stateInVmWithInitial(initial: T) =
    stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000L), initial)