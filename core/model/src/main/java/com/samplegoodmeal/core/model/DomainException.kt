package com.samplegoodmeal.core.model

sealed class DomainException(cause: Throwable? = null) : Exception(cause)

data class ApiException(val statusCode: String) : DomainException()
class NoInternetException : DomainException()
class UnexpectedException(cause: Throwable) : DomainException(cause)