package com.samplegoodmeal.core.model

/**
 * Domain model of a detailed user.
 */
data class User(
    val sortingPreference: SortingPreference,
) {
    enum class SortingPreference {
        DEFAULT, ALPHABETIC
    }
}