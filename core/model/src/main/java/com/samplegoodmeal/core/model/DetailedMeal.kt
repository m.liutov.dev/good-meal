package com.samplegoodmeal.core.model

/**
 * Domain model of a detailed meal.
 */
data class DetailedMeal(
    val id: String,
    val title: String,
    val imageUrl: String,
    val videoUrl: String,
    val instructions: String,
    val tags: List<String>,
)