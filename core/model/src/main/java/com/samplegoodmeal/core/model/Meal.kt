package com.samplegoodmeal.core.model

/**
 * Domain model of a meal.
 */
data class Meal(
    val id: String,
    val title: String,
    val imageUrl: String,
)