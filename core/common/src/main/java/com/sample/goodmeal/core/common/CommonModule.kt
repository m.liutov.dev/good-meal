package com.sample.goodmeal.core.common

import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers

@Module
@InstallIn(SingletonComponent::class)
object CommonModule {

    @Provides
    @Reusable
    fun provideDispatchersProvider(): DispatchersProvider = DispatchersProvider(
        Dispatchers.IO, Dispatchers.Default, Dispatchers.Main
    )

}