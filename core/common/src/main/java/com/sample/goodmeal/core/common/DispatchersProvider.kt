package com.sample.goodmeal.core.common

import kotlinx.coroutines.CoroutineDispatcher

/**
 * Provides different coroutine dispatchers without hardcoding them to specific ones.
 */
data class DispatchersProvider(
    val io: CoroutineDispatcher,
    val default: CoroutineDispatcher,
    val main: CoroutineDispatcher,
)