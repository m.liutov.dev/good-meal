plugins {
    id("goodmeal.android.library")
    id("goodmeal.hilt")
}

android {
    namespace = "com.sample.goodmeal.core.common"
}