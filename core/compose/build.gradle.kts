plugins {
    id("goodmeal.android.library")
    id("goodmeal.android.library.compose")
}

android {
    namespace = "com.sample.goodmeals.core.compose"
}

dependencies {
    with(libs) {
        implementation(bundles.compose)
        implementation(fragment)
    }
}