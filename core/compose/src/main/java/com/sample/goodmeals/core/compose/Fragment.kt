package com.sample.goodmeals.core.compose

import android.view.View
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import com.sample.goodmeals.core.compose.ui.GoodMealTheme

fun Fragment.setThemedContent(content: @Composable () -> Unit): View =
    ComposeView(requireContext()).apply {
        setContent {
            GoodMealTheme {
                content()
            }
        }
    }
