package com.sample.goodmeals.core.compose

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

/**
 * Error box for displaying error messages with ability to retry.
 */
@Composable
fun ErrorBox(message: String, onRetry: () -> Unit, modifier: Modifier = Modifier) {
    Column(
        modifier = modifier, horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        Text(text = message, style = MaterialTheme.typography.h4, textAlign = TextAlign.Center)
        Button(
            onClick = onRetry
        ) {
            Text(
                text = stringResource(id = R.string.error_retry_btn),
                style = MaterialTheme.typography.button
            )
        }
    }
}

@Preview
@Composable
private fun ErrorBoxPreview() {
    ErrorBox(message = "Error message", onRetry = { })
}