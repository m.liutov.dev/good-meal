# GoodMeal

This is a sample project, the aim of which is to demonstrate the usage of some technologies, as well as an overall approach to the app's architecture. To anyone checking it's advised to go through this README file to better understand the decisions I've made during its development.

## Architecture

### MVVM, Reactive

The project uses MVVM, which is nowadays standard for Android applications. The state of screens is always represented using a single class (with nested classes possible). The state of the screen is created in a reactive way from 1/many state producers (typically `StateFlow`), which ensures that all state changes are reflected without the need to manually refresh (recreate) it.

Another alternative to consider for projects is MVI, however, I feel that it can be to strict, and I'm already using some parts/ideas of it in the reactive MVVM (single state object, combine functions can be considered to be state reducers).

The project follows the Clean architecture with a separation of presentation, domain, data layers and models. The domain layer is skipped when it would act as a Middle-man without added value. The architecture could be made even stricter by making the domain layer independent of data layer (having repository interfaces in Domain, implementing and providing via Hilt in data layer), however it felt like an overkill for this project to use it.

### Modularization

The app is split into modules based on layers, as well as features. This approach splits the app into logical components while minimizing the risk of circular dependencies. An overview of used modules:

- `build-logic` - used for sharing convention plugins, in order to share code in gradle scripts. Added to project as included build, which avoids cache invalidation problem of `buildSrc`.
- `core-common` - utility classes to be used by any module (e.g. to provide dispatchers, standard library extensions...)
- `core-compose` - common code for modules using compose (e.g. shared composables, utils)
- `core-model` - domain models of the application
- `core-network` - classes for working with network, provides a shared `OkHttpClient` (sharing base instance is recommended to optimize threads and memory usage) as well as basic error handling adapter.
- `core-presentation` - utility classes to be used in Fragments and ViewModels (e.g. events, basic error message mapping)
- `data-meals` - provides a repository for meals
- `data-user` - provides a dummy repository for user, present in project to have more than 1 repository (and create a use case).
- `domain-meals` - provides business logic of meals in a form of use-cases.
- `feature-meals` - feature module implementing the UI and presentation layer for meal-related screens.

## Navigation

The main options for the implementation of navigation were Navigation Components with Fragment or full Composable navigation. I've decided to use the first (hybrid) approach as I think that it provides more flexibility. For example, composable navigation doesn't support transition animations yet, which would not be acceptable for most production projects.

## Event handling

There're a lot of different suggestions on how to handle events properly. I believe that events should be modeled as state as much as possible (e.g. for snackbars, dialogs it works well), but still in some cases fire-and-forget approach is desired, for example for navigation. 

For such events a `Channel` can be used, however it can introduce bugs in some cases without using the `Dispatchers.main.immediate`. In this project, I've used the old `SingleLiveEvent` approach, as I think that it's short and easy to write, easy to test and work well. The actual selection of a way to handle events should be selected based on specific project needs.

## Compose

In current state, the Compose code in the project is not optimized (too many recompositions), but the performance is not affected, so I would optimize it once performance becomes an issue. Some of the fixes to make:
- Use `ImmutableList` instead of `List` in composable arguments
- Re-check stability of arguments in composables, especially if models from other modules are used (because compose compiler can't infer stability for those)

## Summary

The aim of the project was to demonstrate a way to write a testable, maintainable and scalable code for medium to big-sized projects. Specific architecture and technologies should be selected according to the actual needs of the project (size, budget), so some parts might be relaxed, and some might be even stricter.
