plugins {
    id("goodmeal.android.library")
    id("goodmeal.hilt")
}

android {
    namespace = "com.sample.goodmeal.domain.meals"
}

dependencies {
    implementation(projects.core.model)
    implementation(projects.core.common)
    implementation(projects.data.meals)
    implementation(projects.data.user)
    testImplementation(libs.bundles.unitTesting)
}