package com.sample.goodmeal.domain.meals

import com.sample.goodmeal.core.common.DispatchersProvider
import com.sample.goodmeal.data.meals.MealsRepository
import com.sample.goodmeal.data.user.UserRepository
import com.samplegoodmeal.core.model.Meal
import com.samplegoodmeal.core.model.User
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Gets a list of meals based on user's sorting preference.
 */
class GetUserMealsListUseCase @Inject constructor(
    private val mealsRepository: MealsRepository,
    private val userRepository: UserRepository,
    private val dispatchersProvider: DispatchersProvider,
) {
    suspend operator fun invoke(searchTerm: String?): List<Meal> =
        withContext(dispatchersProvider.default) {
            val foundMeals = mealsRepository.searchMeals(searchTerm)
            when (userRepository.getUser().sortingPreference) {
                User.SortingPreference.DEFAULT -> foundMeals
                User.SortingPreference.ALPHABETIC -> foundMeals.sortedBy { it.title }
            }
        }
}