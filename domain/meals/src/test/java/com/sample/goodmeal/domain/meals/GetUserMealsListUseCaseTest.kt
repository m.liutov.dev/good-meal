package com.sample.goodmeal.domain.meals

import com.sample.goodmeal.core.common.DispatchersProvider
import com.sample.goodmeal.data.meals.MealsRepository
import com.sample.goodmeal.data.user.UserRepository
import com.samplegoodmeal.core.model.Meal
import com.samplegoodmeal.core.model.User
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class GetUserMealsListUseCaseTest {

    private val testDispatcher = UnconfinedTestDispatcher()
    private val dispatchersProvider =
        DispatchersProvider(testDispatcher, testDispatcher, testDispatcher)

    private lateinit var mealsRepository: MealsRepository
    private lateinit var userRepository: UserRepository

    private lateinit var getUserMealsList: GetUserMealsListUseCase

    @Before
    fun setup() {
        mealsRepository = mockk()
        userRepository = mockk()
        coEvery { mealsRepository.searchMeals(SEARCH_TERM) } returns MEALS_DEFAULT

        getUserMealsList =
            GetUserMealsListUseCase(mealsRepository, userRepository, dispatchersProvider)
    }

    @Test
    fun defaultSorting_correctResult() = runTest {
        coEvery { userRepository.getUser() } returns User(User.SortingPreference.DEFAULT)

        val actual = getUserMealsList(SEARCH_TERM)

        assertEquals(MEALS_DEFAULT, actual)
    }

    @Test
    fun alphaSorting_correctResult() = runTest {
        coEvery { userRepository.getUser() } returns User(User.SortingPreference.ALPHABETIC)

        val actual = getUserMealsList(SEARCH_TERM)

        assertEquals(MEALS_ALPHA, actual)
    }

    companion object {
        private const val SEARCH_TERM = ""
        private val MEALS_DEFAULT = listOf(
            Meal("id1", "c", "img"),
            Meal("id2", "a", "img"),
            Meal("id3", "b", "img"),
        )
        private val MEALS_ALPHA = listOf(
            Meal("id2", "a", "img"),
            Meal("id3", "b", "img"),
            Meal("id1", "c", "img"),
        )
    }
}