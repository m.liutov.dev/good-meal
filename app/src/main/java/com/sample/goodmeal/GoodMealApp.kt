package com.sample.goodmeal

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class GoodMealApp : Application()