plugins {
    id("goodmeal.android.application")
    id("goodmeal.android.application.compose")
    id("goodmeal.hilt")
    with(libs.plugins) {
        alias(kotlin.kapt)
        alias(navigation)
    }
}

android {
    namespace = "com.sample.goodmeal"

    defaultConfig {
        applicationId = "com.sample.goodmeal"
        versionCode = 1
        versionName = "1.0"

        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildFeatures {
        viewBinding = true
    }

    signingConfigs {
        create("release") {
            // In reality keys should be kept secret (e.g. as environment variables)
            keyAlias = "key0"
            keyPassword = "goodmealsample"
            storeFile = File("$rootDir/keys/keystore.jks")
            storePassword = "goodmealsample"
        }
    }
    buildTypes {
        release {
            isDebuggable = true
            signingConfig = signingConfigs.getByName("release")
        }
        debug {
            signingConfig = signingConfigs.getByName("debug")
        }
    }
}

dependencies {
    implementation(projects.feature.meals)
    implementation(projects.core.model)
    with(libs) {
        implementation(androidCore)
        implementation(fragment)
        implementation(bundles.navigation)
        implementation(bundles.compose)
    }
}