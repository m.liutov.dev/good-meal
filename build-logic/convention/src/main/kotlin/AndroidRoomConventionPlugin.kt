import com.sample.goodmeal.versionCatalog
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.configure
import org.gradle.kotlin.dsl.dependencies
import org.jetbrains.kotlin.gradle.plugin.KaptExtension

class AndroidRoomConventionPlugin : Plugin<Project> {

    override fun apply(target: Project) {
        with(target) {
            pluginManager.apply("org.jetbrains.kotlin.kapt")

            extensions.configure<KaptExtension> {
                arguments {
                    arg("room.schemaLocation", "$projectDir/schemas")
                }
            }

            val libs = versionCatalog()
            dependencies {
                "implementation"(libs.findBundle("room").get())
                "kapt"(libs.findBundle("room-kapt").get())
            }
        }
    }
}