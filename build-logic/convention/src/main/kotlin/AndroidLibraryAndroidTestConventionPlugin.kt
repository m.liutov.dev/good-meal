import com.android.build.gradle.LibraryExtension
import com.sample.goodmeal.versionCatalog
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.configure
import org.gradle.kotlin.dsl.dependencies

class AndroidLibraryAndroidTestConventionPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            pluginManager.apply("com.android.library")
            extensions.configure<LibraryExtension> {
                defaultConfig {
                    testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
                }
                val libs = versionCatalog()
                dependencies {
                    "androidTestImplementation"(libs.findBundle("unitTestingAndroid").get())
                }
            }
        }
    }
}