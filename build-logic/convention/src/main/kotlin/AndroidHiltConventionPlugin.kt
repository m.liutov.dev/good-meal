import com.sample.goodmeal.versionCatalog
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies

class AndroidHiltConventionPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            with(pluginManager) {
                apply("dagger.hilt.android.plugin")
                apply("org.jetbrains.kotlin.kapt")
            }

            val libs = versionCatalog()
            dependencies {
                "implementation"(libs.findBundle("hilt").get())
                "kapt"(libs.findBundle("hilt-kapt").get())
            }

        }
    }
}