plugins {
    `kotlin-dsl`
    `java-gradle-plugin`
}

dependencies {
    compileOnly(libs.android.gradlePlugin)
    compileOnly(libs.kotlin.gradlePlugin)
}

gradlePlugin {
    plugins {
        register("androidApplicationCompose") {
            id = "goodmeal.android.application.compose"
            implementationClass = "AndroidApplicationComposeConventionPlugin"
        }
        register("androidLibraryCompose") {
            id = "goodmeal.android.library.compose"
            implementationClass = "AndroidLibraryComposeConventionPlugin"
        }
        register("androidApplication") {
            id = "goodmeal.android.application"
            implementationClass = "AndroidApplicationConventionPlugin"
        }
        register("androidLibrary") {
            id = "goodmeal.android.library"
            implementationClass = "AndroidLibraryConventionPlugin"
        }
        register("hilt") {
            id = "goodmeal.hilt"
            implementationClass = "AndroidHiltConventionPlugin"
        }
        register("room") {
            id = "goodmeal.room"
            implementationClass = "AndroidRoomConventionPlugin"
        }
        register("androidLibraryAndroidTest") {
            id = "goodmeal.android.library.androidtest"
            implementationClass = "AndroidLibraryAndroidTestConventionPlugin"
        }
    }
}
